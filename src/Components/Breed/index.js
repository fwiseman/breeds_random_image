
import React from 'react';
import style from "./Breed.css";



export default class Breed extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            url_image:null
        }
    }
    componentWillReceiveProps(nextProps){
        fetch(`https://dog.ceo/api/breed/${nextProps.url_image}/images/random`).then(response => response.json())
        .then(response => {
          if (response.status === 'success') {
            this.setState((state, props) => {
              return {url_image: response.message};
            });
          }
        });
    }
    
    render(){
        return(
            <section className={style.imgContainer}>
                <div></div>
                <img srcSet={this.state.url_image} />
            </section>
        );
    }
}