import React from 'react';
import style from "./Search.css";



export default class Search extends React.Component {
    constructor(props){
        super(props);
        this.state = {
          breeds:null
        }
        this.get_breed = this.props.get_breed.bind(this);
    }

    componentDidMount() {
      fetch('https://dog.ceo/api/breeds/list/all').then(response => response.json())
        .then(response => {
          if (response.status === 'success') {
            
            this.setState({
              breeds: response.message
            });
            //this.get_breed(Object.key(response.message));
          }
        });
    }
    get_under_list(breed, descents){
      let d = descents.map(descent=>{
        return (<li key={`${breed}/${descent}`} onClick={()=>this.get_breed(`${breed}/${descent}`)}>{descent}</li>);
      });

      return(<li key={`${breed}`}>
        <span onClick={()=>this.get_breed(`${breed}`)}>{breed}</span>
        <ul>{d}</ul></li>
      );
    }

    render_breed_list(){
      if(this.state.breeds === null )return(<div>loading...</div>);
      return Object.keys(this.state.breeds).map(breed=>{
        return(
          (this.state.breeds[breed].length == 0) ?
            (<li key={breed}><span onClick={()=>this.get_breed(`${breed}`)}>{breed}</span></li>)
            :
            (this.get_under_list(breed, this.state.breeds[breed]))
        );
      });
    }

    render() {
      if(!this.state.breeds === null) return (<div>loading...</div>);
      return (
        <ul className={style.primaryButton}>
          {this.render_breed_list()}
        </ul>
      );
    }
  }