import React from "react";
import ReactDOM from "react-dom";
import style from "./main.css";
import Search from './Components/Search';
import Breed from './Components/Breed';

class Index extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      breeds: null,
      url_image:null
      
    }
  }

  get_breed(url_breed) {
    this.setState({
      url_image: url_breed
    });
  }

  render(){
    return <div>
        <div>
          <aside className={style.sideBar}>
            <Search get_breed={this.get_breed.bind(this)}/>
          </aside>
          <article>
            <Breed url_image={this.state.url_image} />
          </article>
        </div>
    </div>;
  };
}

ReactDOM.render(<Index />, document.getElementById("index"));